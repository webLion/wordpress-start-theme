<?php

// Support title in theme
add_theme_support('title-tag');

// Support thumbnail in theme
add_theme_support('post-thumbnails');

// Register menu locations
if(!function_exists('register_nav_locations')) {
    add_action('after_setup_theme', 'register_nav_locations');
    function register_nav_locations() {
        register_nav_menus(array(
            'primary_menu'	=> 'Primary menu'
        ));
    }
}

// Add scripts & styles to head
if(!function_exists('add_assets_to_head')) {
    add_action('wp_head', 'add_assets_to_head');
    function add_assets_to_head() {
        if(is_admin()) return false;
        // styles
        wp_enqueue_style('css', get_template_directory_uri() . '/assets/css/style.min.css');
        // scripts
        wp_enqueue_script('js', get_template_directory_uri() . '/assets/js/script.min.js');
    }
}


// Remove ID from menus item
add_filter('nav_menu_item_id', '__return_false');

// Off REST API
add_filter('rest_enabled', '__return_false');

// Off filters REST API
remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');
remove_action('wp_head', 'rest_output_link_wp_head', 10, 0);
remove_action('template_redirect', 'rest_output_link_header', 11, 0);
remove_action('auth_cookie_malformed', 'rest_cookie_collect_status');
remove_action('auth_cookie_expired', 'rest_cookie_collect_status');
remove_action('auth_cookie_bad_username', 'rest_cookie_collect_status');
remove_action('auth_cookie_bad_hash', 'rest_cookie_collect_status');
remove_action('auth_cookie_valid', 'rest_cookie_collect_status');
remove_filter('rest_authentication_errors', 'rest_cookie_check_errors', 100);

// Off event REST API
remove_action('init', 'rest_api_init' );
remove_action('rest_api_init', 'rest_api_default_filters', 10, 1 );
remove_action('parse_request', 'rest_api_loaded' );

// Off Embeds REST API
remove_action('rest_api_init', 'wp_oembed_register_route');
remove_filter('rest_pre_serve_request', '_oembed_rest_pre_serve_request', 10, 4);

// Remove tags from header
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_oembed_add_discovery_links' );
remove_action('wp_head', 'wp_oembed_add_host_js');
remove_action('wp_head', 'wp_generator');

// Off xmlrpc
add_filter('xmlrpc_enabled', '__return_false');

// Remove smile images
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Remove DNF perfect
add_filter('emoji_svg_url', '__return_false');

// Remove setsrc
add_filter('max_srcset_image_width', create_function('', 'return 1;' ));

// Blacklist WordPress menu class names
if(!function_exists('remove_menu_classes')) {
    function remove_menu_classes(array $classes, $item) {
        $disallowed_class_names = array(
            "menu-item-object-{$item->object}",
            "menu-item-type-{$item->type}",
            "menu-item-{$item->ID}",
            "current-{$item->object}-item",
            "current-{$item->type}-item",
            "current-{$item->object}-parent",
            "current-{$item->type}-parent",
            "current-{$item->object}-ancestor",
            "current-{$item->type}-ancestor",
            'page_item',
            'page_item_has_children',
            "page-item-{$item->object_id}",
            'current_page_item',
            'current_page_parent',
            'current_page_ancestor'
        );
        foreach ($classes as $class) {
            if(in_array($class, $disallowed_class_names)) {
                $key = array_search($class, $classes);
                if (false !== $key) {
                    unset($classes[$key]);
                }
            }
        }
        return $classes;
    }
    add_filter('nav_menu_css_class', 'remove_menu_classes', 10, 4);
}