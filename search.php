<?php get_header();
echo get_search_query();
while(have_posts()) : the_post();
    the_title();
    the_excerpt();
endwhile;
get_footer(); ?>